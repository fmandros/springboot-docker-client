## Borrar Anterior Ejecución ##

$ docker ps -a
$ docker rm 4af0d234522a
$ docker image ls
$ docker image rm springboot/alpine-oraclejdk8


## Ejecutar Spring Boot Dockerizado ##

$ cd springboot-docker-client/
$ docker build -t springboot/alpine-oraclejdk8 .
$ docker run springboot/alpine-oraclejdk8:latest
$ docker ps -a
$ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' 3455a614c7f0
$ curl -i http://172.17.0.2:8080

## Ejecutar Otro Contenedor puerto 8081

$ docker run -d -p 8081:8080 --name microservicio springboot/alpine-oraclejdk8:latest

## Instalas Docker Compose ##

$ curl -L "https://github.com/docker/compose/releases/download/1.23.0-rc3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ chmod +x /usr/local/bin/docker-compose
$ vi /roo/.bash_profile
---
...
# DOCKER COMPOSE
export DOCKER_COMPOSE=/usr/local/bin
export PATH=${PATH}:${DOCKER_COMPOSE}
---
$ source ~/.bash_profile
$ docker-compose --version
---
docker-compose version 1.23.0-rc3, build ea3d406e
---

## Docker Compose: construir en base docker-compose.yml ##

$ cd springboot-docker-client/
$ docker-compose up -d
---
Pulling loadbalancer (dockercloud/haproxy:latest)...
latest: Pulling from dockercloud/haproxy
1160f4abea84: Pull complete
b0df9c632afc: Pull complete
a49b18c7cd3a: Pull complete
Creating springboot-docker-client_microservice2_1_b5425ca8c394 ... done
Creating springboot-docker-client_microservice1_1_e6eaeaec224c ... done
Creating springboot-docker-client_loadbalancer_1_361c5f25f027  ... done
---
$ docker ps -a
---
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS                      PORTS                                   NAMES
70c1f5bcfaa6        dockercloud/haproxy:latest            "/sbin/tini -- docke…"   4 minutes ago       Up 4 minutes                443/tcp, 0.0.0.0:80->80/tcp, 1936/tcp   springboot-docker-client_loadbalancer_1_2ddc29b0acf3
a96733e7b34d        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   4 minutes ago       Up 4 minutes                8080/tcp                                springboot-docker-client_microservice1_1_f9b7bf46ef34
fc8df790d4cb        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   4 minutes ago       Up 4 minutes                8080/tcp                                springboot-docker-client_microservice2_1_351b343004fd
---


## Docker Compose: construir en base docker-composeV2.yml ##

$ cd springboot-docker-client/
$ docker-compose -f docker-composeV2.yml up -d
---
Creating network "springboot-docker-client_default" with the default driver
WARNING: Found orphan containers (springboot-docker-client_microservice1_1_f9b7bf46ef34, springboot-docker-client_microservice2_1_351b343004fd) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
Creating springboot-docker-client_microservice_1_3b594e3129d5 ... done
Recreating springboot-docker-client_loadbalancer_1_2ddc29b0acf3 ... done
---
$ docker ps -a
---
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS                      PORTS                                   NAMES
f11a59b18b4b        dockercloud/haproxy:latest            "/sbin/tini -- docke…"   25 seconds ago      Up 19 seconds               443/tcp, 0.0.0.0:80->80/tcp, 1936/tcp   springboot-docker-client_loadbalancer_1_2ddc29b0acf3
7613e22a3b8b        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   38 seconds ago      Up 29 seconds               8080/tcp                                springboot-docker-client_microservice_1_761c6637fc54
a96733e7b34d        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   9 minutes ago       Up 9 minutes                8080/tcp                                springboot-docker-client_microservice1_1_f9b7bf46ef34
fc8df790d4cb        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   9 minutes ago       Up 9 minutes                8080/tcp                                springboot-docker-client_microservice2_1_351b343004fd
---

## Docker Compose: construir en base docker-composeV2.yml SCALE ##

$ cd springboot-docker-client/
$ docker-compose -f docker-composeV2.yml scale microservice=3
---
WARNING: The scale command is deprecated. Use the up command with the --scale flag instead.
Starting springboot-docker-client_microservice_1_761c6637fc54 ... done
Creating springboot-docker-client_microservice_2_9e137b7b77ab ... done
Creating springboot-docker-client_microservice_3_aed83be28241 ... done
---
$ docker ps -a
---
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS                      PORTS                                   NAMES
1579beaad97b        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   35 seconds ago      Up 19 seconds               8080/tcp                                springboot-docker-client_microservice_2_168661b70029
14f62faf8298        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   35 seconds ago      Up 19 seconds               8080/tcp                                springboot-docker-client_microservice_3_8ef583b46ea3
f11a59b18b4b        dockercloud/haproxy:latest            "/sbin/tini -- docke…"   2 minutes ago       Up About a minute           443/tcp, 0.0.0.0:80->80/tcp, 1936/tcp   springboot-docker-client_loadbalancer_1_2ddc29b0acf3
7613e22a3b8b        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   2 minutes ago       Up 2 minutes                8080/tcp                                springboot-docker-client_microservice_1_761c6637fc54
a96733e7b34d        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   10 minutes ago      Up 10 minutes               8080/tcp                                springboot-docker-client_microservice1_1_f9b7bf46ef34
fc8df790d4cb        springboot/alpine-oraclejdk8:latest   "java -jar springboo…"   10 minutes ago      Up 10 minutes               8080/tcp                                springboot-docker-client_microservice2_1_351b343004fd
---